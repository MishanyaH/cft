﻿#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>

class file_sort
{
public:
	file_sort();
	file_sort(int args_number, char *args[]);
	file_sort(std::string read_file, std::string output_file, std::string type_content, std::string type_of_sorting);
	void sort_the_file();
	~file_sort();
private:
	static const int max_size = 100;
	int current_size;
	std::string read_file_name,
		write_file_name,
		type_of_content_string,
		type_of_sorting_string,
		string_text[max_size];
	int integer_text[max_size];
	bool are_normal_arguments(std::string fileContent, std::string sortType);
	int get_current_size();
	bool is_type_of_content_number();
	bool is_all_content_number();
	bool a_bigger_b(int a, int b);
	bool a_less_b(int a, int b);
	bool a_bigger_b(std::string a, std::string b);
	bool a_less_b(std::string a, std::string b);
	bool read_file();
	void write_in_file();
	void sort_the_content();
	void sort_number(bool(file_sort::*needsToBeMoved)(int, int));
	void sort_string(bool(file_sort::*needsToBeMoved)(std::string, std::string));
	void make_to_number();
	bool file_sort::is_number(const std::string& s);
};

