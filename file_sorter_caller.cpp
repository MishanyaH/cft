﻿#include "file_sorter.h"

int main(int args_number, char *args[]) {
	file_sort file_sort(args_number, args);
	file_sort.sort_the_file();
	std::cout << "Your result is in " << args[2] << "\n";
}