﻿#include "file_sorter.h"

/*
Usage:
sort-it.exe in.txt out.txt -i -a (Ascending insertion sort for integers)
sort-it.exe in.txt out.txt -i -d (Descending insertion sort for integers)
sort-it.exe in.txt out.txt -s -a (Ascending insertion sort for strings)
sort-it.exe in.txt out.txt -s -d (Descending insertion sort for strings)
*/

bool file_sort::a_bigger_b(int a, int b)
{
	return (a > b);
}
bool file_sort::a_less_b(int a, int b)
{
	return (a < b);
}
bool file_sort::a_less_b(std::string a, std::string b)
{
	return a.compare(b) < 0;
}
bool file_sort::a_bigger_b(std::string a, std::string b)
{
	return a.compare(b) > 0;
}

file_sort::file_sort(std::string read_file, std::string output_file, std::string type_content, std::string type_of_sorting)
{
	this->read_file_name = read_file;
	this->write_file_name = output_file;
	this->type_of_content_string = type_content;
	this->type_of_sorting_string = type_of_sorting;
}
file_sort::file_sort(int args_number, char *args[])
{
	if (args_number != 5) {
		std::cout << "Wrong arguments quantity!\n";
		exit(0);
	}
	else {
		*this = file_sort(args[1], args[2], args[3], args[4]);
	}
}
file_sort::file_sort()
{
	std::cout << "No any arguments!\n";
	exit(0);
}
file_sort::~file_sort()
{
}
void file_sort::sort_the_file()
{
	if (!read_file()) {
		std::cout << "File doesn't exist!\n";
	}
	else {
		read_file();
		sort_the_content();
		write_in_file();
	}
}
void file_sort::sort_the_content()
{	
	if (!are_normal_arguments(type_of_content_string, type_of_sorting_string)) {
		std::cout << "Wrong arguments!\n";
		exit(0);
	}
	current_size = get_current_size();
	if (is_type_of_content_number()) {
		make_to_number();
		if (type_of_sorting_string == "-a")
			sort_number(&file_sort::a_less_b);
		else
			sort_number(&file_sort::a_bigger_b);
	}
	else
		if (type_of_sorting_string == "-a")
			sort_string(&file_sort::a_less_b);
		else
			sort_string(&file_sort::a_bigger_b);
}
void file_sort::sort_string(bool (file_sort::*comprassion_function)(std::string, std::string))
{
	for (int i = 1; i < current_size; i++) {
		std::string current_element = string_text[i];
		int j;
		for (j = i; j > 0 && (this->*comprassion_function)(current_element, string_text[j - 1]); j--)
			string_text[j] = string_text[j - 1];
		if ((this->*comprassion_function)(current_element, string_text[j]))
			string_text[j] = current_element;
	}
}
void file_sort::sort_number(bool (file_sort::*comprassion_function)(int, int))
{
	for (int i = 1; i < current_size; i++) {
		int current_element = integer_text[i];
		int j;
		for (j = i; j > 0 && (this->*comprassion_function)(current_element, integer_text[j - 1]); j--)
			integer_text[j] = integer_text[j - 1];
		if ((this->*comprassion_function)(current_element, integer_text[j]))
			integer_text[j] = current_element;
	}
}
bool file_sort::read_file()
{
	std::ifstream file_in(read_file_name);
	if (!file_in.is_open()) {
		return false;
	}
	else {
		for (int i = 0; !file_in.eof(); i++) {
			file_in >> string_text[i];
		}
		return true;
	}
}
void file_sort::write_in_file()
{
	std::ofstream file_out(write_file_name);
	for (int i = 0; i < current_size; i++)
		if (is_type_of_content_number())
			file_out << integer_text[i] << std::endl;
		else
			file_out << string_text[i] << std::endl;
}
void file_sort::make_to_number()
{
	if (!is_all_content_number()) {
		std::cout << "Content contains not number!";
		exit(0);
	}
	for (int i = 0; i < current_size; i++) {
		std::istringstream buf(string_text[i]);
		buf >> integer_text[i];

	}
}
int file_sort::get_current_size()
{	
	for (int i = 0; i < max_size; i++) {
		if (string_text[i].empty())
			return i;
	}
}
bool file_sort::is_type_of_content_number()
{
	return type_of_content_string != "-s";
}
bool file_sort::is_all_content_number()
{	
	for (int i = 0; i < current_size; i++) {
		if(!is_number(string_text[i]))
			return false;
		}
	return true;
}
bool file_sort::are_normal_arguments(std::string type_content, std::string type_of_sorting) {
	if (type_content != "-s" && type_content != "-i")
		return false;
	if (type_of_sorting != "-a" && type_of_sorting != "-d")
		return false;
	return true;
}
bool file_sort::is_number(const std::string& string)
{
	std::string::const_iterator it = string.begin();
	while (it != string.end() && isdigit(*it)) ++it;
	return !string.empty() && it == string.end();
}
